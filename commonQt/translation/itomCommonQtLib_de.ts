<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="67"/>
        <source>Tried to scale unscaleable unit</source>
        <translation type="unfinished">Es wurde versucht eine unskalierbare Einheit zu skalieren</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="71"/>
        <source>No unit specified</source>
        <translation type="unfinished">Keine Einheiten festgelegt</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="202"/>
        <source>Pluginname undefined. No xml file loaded</source>
        <translation type="unfinished">Der Plugin-Name ist nicht definiert. Es wurde keine XML-Datei geladen</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="239"/>
        <source>ParamList not inialized properly</source>
        <translation type="unfinished">Die ParamList wurde nicht richtig inizialisiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="481"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="697"/>
        <source>Can&apos;t open xml file</source>
        <translation type="unfinished">XML-Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="769"/>
        <source>%1
Autosave parameter %2 not found</source>
        <translation type="unfinished">%1
Der Autosave-Parameter %2 wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="773"/>
        <source>XML-Import warnings:
Autosave parameter %1 not found</source>
        <translation type="unfinished">Warnung beim XML-Import:
Der Autosave-Parameter %1 wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="800"/>
        <source>%1
Obsolete parameter %2</source>
        <translation type="unfinished">%1
Parameter &apos;%2&apos; veraltet</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="804"/>
        <source>XML-Import warnings:
Obsolete parameter %1</source>
        <translation type="unfinished">Warnung beim XML-Import:
Der Parameter &apos;%1&apos; ist veraltet</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="813"/>
        <source>%1
Parameter %2 not autosave</source>
        <translation type="unfinished">%1
Der Parameter &apos;%2&apos; ist nicht Autosave</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="817"/>
        <source>XML-Import warnings:
Parameter %1 not autosave</source>
        <translation type="unfinished">Warnung beim XML-Import:
Der Parameter &apos;%1&apos; ist nicht Autosave</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="842"/>
        <source>%1
Parameter not loadable %2</source>
        <translation type="unfinished">%1
Der Parameter &apos;%2&apos; kann nicht geladen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="846"/>
        <source>XML-Import warnings:
Parameter not loadable %1</source>
        <translation type="unfinished">Warnung beim XML-Import:
Der Parameter &apos;%1&apos; kann nicht geladen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="856"/>
        <source>%1
Type conflict for %2</source>
        <translation type="unfinished">%1
Typkonflikt für &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="860"/>
        <source>XML-Import warnings:
Type conflict for %1</source>
        <translation type="unfinished">Warnung beim XML-Import:
Typkonflikt für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="937"/>
        <source>Save object failed: Type not supported</source>
        <translation type="unfinished">Fehler beim Speichern des Objekts: Der Typ wird nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1231"/>
        <source>Save object failed: Invalid object handle</source>
        <translation type="unfinished">Fehler beim Speichern des Objekts: Ungültiges Objekt-Handle</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1236"/>
        <source>Save object failed: Object seems empty</source>
        <translation type="unfinished">Fehler beim Speichern des Objekts: Das Objekt scheint leer zu sein</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1271"/>
        <source>Save object failed: File not writeable</source>
        <translation type="unfinished">Fehler beim Speichern des Objekts: Das Objekt ist nicht beschreibbar</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1366"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1395"/>
        <source>Load object warning: Metadata &quot; %1 &quot; for %2 missing</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Bei Attribut &apos;%2&apos; fehlt MetaData &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1431"/>
        <source>Load object failed: Number of dims smaller 2</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Anzahl der Dimensionen ist kleiner als 2</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1439"/>
        <source>Not enough memory to alloc sizes vector</source>
        <translation type="unfinished">Für diese Vektorgröße ist nicht genug Arbeitsspeicher verfügbar</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1455"/>
        <source>Load object failed: dimension size missing</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Dimensionsgröße wurde nicht angegeben</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1469"/>
        <source>Load object failed: dimX not specified</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: &apos;DimX&apos; ist nicht spezifiziert</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1481"/>
        <source>Load object failed: dimY not specified</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: &apos;DimY&apos; ist nicht spezifiziert</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1556"/>
        <source>Load object failed: type not supported</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Typ wird nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1565"/>
        <source>Load object failed: Error during allocating memory</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Fehler beim Speicherallozieren</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1610"/>
        <source>Load object failed: file corrupted at metaData (v1.0)</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Datei ist beschädigt (v1.0)</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1617"/>
        <source>Load object warning: file has invalid metaData for v1.0</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Die Datei hat ungültige MetaData für v1.0</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1635"/>
        <source>Load object warning: DoubleExportType for v1.0 invalid</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Ungültiger &apos;DoubleExportType&apos; für v1.0</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1640"/>
        <source>Load object warning: DoubleExportType for v1.0 missing</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Fehlender &apos;DoubleExportType&apos; für v1.0</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1674"/>
        <source>Load object warning: MetaData for %1 missing</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Fehlende MetaData für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1707"/>
        <source>Load object warning: MetaData for dimX missing</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Fehlende MetaData für &apos;DimX&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1737"/>
        <source>Load object warning: MetaData for dimY missing</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Fehlende MetaData für &apos;DimY&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1767"/>
        <source>Load object warning: MetaData for values missing</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Fehlende MetaData-Werte</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1796"/>
        <source>Load object warning: MetaData import for Rotation Matrix failed</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Der MetaData-Import für die Rotationsmatrix schlug fehl</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1831"/>
        <source>Load object failed: file corrupted at tagSpace (v1.0)</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Datei bei &apos;TagSpace&apos; beschädigt (v1.0)</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1836"/>
        <source>Load object failed: tag space not at expected position. Got %1 instead</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: &apos;TagSpace&apos; an falscher Position. (Position %1)</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1849"/>
        <source>Load object failed: tags Space invalid</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: &apos;TagSpace&apos; ist ungültig</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1887"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1893"/>
        <source>Load object warning: invalid tagType found</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Ungültiger &apos;TagTyp&apos; gefunden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1899"/>
        <source>Load object warning: tagsSpace invalid</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: Ungültiges &apos;TagsSpace&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1938"/>
        <source>Load object failed: dataSpace missing</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Fehlendes &apos;DataSpace&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1943"/>
        <source>Load object failed: dataSpace not at expected position. Got %1 instead</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: &apos;DataSpace&apos; an falscher Position. (Position %1)</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1953"/>
        <source>Load object warning: dataSpace and dataObject are not equal</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: &apos;DataSpace&apos; und &apos;DataObject&apos; sind nicht gleich</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1958"/>
        <source>Load object warning: dataSpace attributes corrupted</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: die Attribute von &apos;DataSpace&apos; sind defekt</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1971"/>
        <source>Load object warning: dataSpace for a plane corrupted. Got %1 instead of %2 bytes</source>
        <translation type="unfinished">Warnung beim Laden des Objekts: &apos;DataSpace&apos; für eine Ebene ungültig. Statt %2 wurden %1 Byte(s) übergeben</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1982"/>
        <source>Load object failed: dataStream ended before finished reading</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: &apos;DataStream&apos; unerwartet zuende</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2012"/>
        <source>Load object failed: Invalid object handle</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Ungültiges Objekt-Handle</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2039"/>
        <source>Load object failed: file not readable or does not exists</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Datei kann nicht geöffnet werden oder exisitert nicht</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2053"/>
        <source>Load object failed: file seems corrupt</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Datei scheint defekt zu sein</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2062"/>
        <source>Load object failed: wrong xml version</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Falsche XML-Version</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2071"/>
        <source>Load object failed: wrong document encoding</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Falscher Dokumenten-Encoder</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2077"/>
        <source>Load object failed: unexpected file ending</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Datei endet unerwartet</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2096"/>
        <source>Load object failed: file is no itomDataObjectFile</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Datei ist kein &apos;itomDataObjectFile&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2124"/>
        <source>Load object failed: illegal format version</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Ungültige Formatversion</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2129"/>
        <source>Load object failed: object header not valied</source>
        <translation type="unfinished">Fehler beim Laden des Objekts: Die Objekt-Header ist ungültig</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="64"/>
        <source>No or invalid plugin given.</source>
        <translation type="unfinished">Kein oder ein ungültiges Plugin übergeben.</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="78"/>
        <source>No or invalid plugin given</source>
        <translation type="unfinished">Kein oder ein ungültiges Plugin übergeben</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="155"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="171"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="176"/>
        <source>Timeout while waiting for answer from camera.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Kamera.</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="205"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="329"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="406"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="434"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="464"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="494"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="525"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="555"/>
        <source>No camera available</source>
        <translation type="unfinished">Keine Kamera verfügbar</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="214"/>
        <source>Error invoking getParam</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;getParam&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="225"/>
        <source>No parameter can be returned if timeout = 0</source>
        <translation type="unfinished">Bei Timeout = 0 können keine Bildparameter zurückgegeben werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="244"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="301"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="359"/>
        <source>no camera available</source>
        <translation type="unfinished">Keine Kamera verfügbar</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="308"/>
        <source>Error invoking startDevice</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;startDevice&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="336"/>
        <source>Error invoking stopDevice</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;stopDevice&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="366"/>
        <source>Error invoking acquire</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;Acquire&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="413"/>
        <source>Error invoking enableAutoGrabbing</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;enableAutoGrabbing&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="441"/>
        <source>Error invoking disableAutoGrabbing</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;disableAutoGrabbing&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="471"/>
        <source>Error invoking setAutoGrabbingInterval</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;setAutoGrabbingInterval&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="501"/>
        <source>Error invoking getVal</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;getVal&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="532"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="568"/>
        <source>Error invoking copyVal</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;copyVal&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="581"/>
        <source>No image parameters can be returned if timeout = 0</source>
        <translation type="unfinished">Bei Timeout = 0 können keine Bildparameter zurückgegeben werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="639"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="676"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="712"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="743"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="770"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="812"/>
        <source>No actuator available</source>
        <translation type="unfinished">Kein Motor verfügbar</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="651"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="719"/>
        <source>Error invoking setPosRel</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;setPosRel&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="688"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="750"/>
        <source>Error invoking setPosAbs</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;setPosAbs&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="781"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="824"/>
        <source>Error invoking getPos</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;getPos&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="792"/>
        <source>No position value can be returned if timeout = 0</source>
        <translation type="unfinished">Bei Timeout = 0 kann kein Positionswert zurückgegeben werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="835"/>
        <source>No position value(s) can be returned if timeout = 0</source>
        <translation type="unfinished">Bei Timeout = 0 kann kein Positionswert zurückgegeben werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="861"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="867"/>
        <source>Failed to ask for number of axes of actuator</source>
        <translation type="unfinished">Bei der Abfrage der Achsenanzahl des Motors ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="252"/>
        <source>error invoking setParam</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;setParam&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="644"/>
        <source>Error during setPosRel: Vectors differ in size</source>
        <translation type="unfinished">Fehler bei setPosRel: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="681"/>
        <source>Error during setPosAbs: Vectors differ in size</source>
        <translation type="unfinished">Fehler bei setPosAbs: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="49"/>
        <source>parameter vector is not initialized</source>
        <translation type="unfinished">Der Parametervektor wurde nicht initialisiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="76"/>
        <source>mandatory parameter vector is not initialized</source>
        <translation type="unfinished">Der Pflichtparametervektor wurde nicht inistalisiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="84"/>
        <source>optional parameter vector is not initialized</source>
        <translation type="unfinished">Der optionale Parametervektor wurde nicht inistialisiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="92"/>
        <source>output parameter vector is not initialized</source>
        <translation type="unfinished">Der Ausgabeparametervektor wurde nicht initialisiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="140"/>
        <location filename="../../common/sources/helperCommon.cpp" line="187"/>
        <source>parameter &apos;%1&apos; cannot be found in given parameter vector</source>
        <translation type="unfinished">Der Parameter &apos;%1&apos; wurde nicht im gegebenen Parametervektor gefunden</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="314"/>
        <location filename="../../common/sources/helperCommon.cpp" line="508"/>
        <source>name of requested parameter is empty.</source>
        <translation type="unfinished">Der Name des gesuchten Parameters ist leer.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="333"/>
        <source>the parameter name &apos;%1&apos; is invald</source>
        <translation type="unfinished">Der Parametername &apos;%1&apos; ist ungültig</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="371"/>
        <source>array index of parameter out of bounds.</source>
        <translation type="unfinished">Array-Index des Parameters außerhalb der Beschränkung.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="382"/>
        <location filename="../../common/sources/helperCommon.cpp" line="550"/>
        <source>given index of parameter name ignored since parameter is no array type</source>
        <translation type="unfinished">Der Index des Parameters wurde ignoriert, da der Parameter kein Array-Typ ist</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="397"/>
        <location filename="../../common/sources/helperCommon.cpp" line="639"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished">Der Parameter wurde nicht in &apos;m_params&apos; gefunden.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="527"/>
        <source>the parameter name &apos;%1&apos; is invalid</source>
        <translation type="unfinished">Der Parametername &apos;%1&apos; ist ungültig</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="571"/>
        <location filename="../../common/sources/helperCommon.cpp" line="599"/>
        <source>The type of the new value does not fit to the given array or list type.</source>
        <translation type="unfinished">Der Typ des übergebenen Werts entspricht nicht dem gegebenen Array- oder List-Typ.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="589"/>
        <location filename="../../common/sources/helperCommon.cpp" line="617"/>
        <source>array index out of bounds.</source>
        <translation type="unfinished">Array-Index liegt außerhalb des Bereichs.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="625"/>
        <source>The parameter is either no array or list, or the type of the new value does not correspond to this parameter.</source>
        <translation type="unfinished">Der Parameter ist entweder kein Array oder Liste oder der Typ des neuen Werts entspricht nicht diesem Parameter.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="690"/>
        <source>invalid parameter name</source>
        <translation type="unfinished">Ungültiger Parametername</translation>
    </message>
</context>
<context>
    <name>ito::AbstractAddInConfigDialog</name>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="78"/>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="83"/>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="151"/>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Zeiger des Plugins ist ungültig.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="97"/>
        <source>Error while setting parameter &apos;%1&apos;</source>
        <translation type="unfinished">Fehler beim Parametersetzen &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="101"/>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="162"/>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="115"/>
        <source>Warning while setting parameter &apos;%1&apos;</source>
        <translation type="unfinished">Warnung beim Parametersetzen &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="119"/>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="173"/>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Warnung beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="146"/>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="198"/>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Plugin-Instanz.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="211"/>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="222"/>
        <source>Warning while execution</source>
        <translation type="unfinished">Warnung bei der Ausführung</translation>
    </message>
</context>
<context>
    <name>ito::AbstractAddInDockWidget</name>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="77"/>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="82"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="130"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="238"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="296"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="346"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="393"/>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Zeiger des Plugins ist ungültig.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="88"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="136"/>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="99"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="147"/>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Warnung beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="125"/>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="172"/>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Plugin-Instanz.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="185"/>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="196"/>
        <source>Warning while execution</source>
        <translation type="unfinished">Warnung bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="220"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="278"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="334"/>
        <source>setActuatorPosition can only be called for actuator plugins</source>
        <translation type="unfinished">Der Parameter &apos;setActuatorPosition&apos; kann nur für Motor-Plugins aufgerufen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="232"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="290"/>
        <source>slot &apos;%1&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;%1&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="244"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="302"/>
        <source>Error while calling %1</source>
        <translation type="unfinished">Fehler beim Aufruf von %1</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="255"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="313"/>
        <source>Warning while calling %1</source>
        <translation type="unfinished">Warnung beim Aufruf von %1</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="340"/>
        <source>slot &apos;requestStatusAndPosition&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;requestStatusAndPosition&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="352"/>
        <source>Error while calling &apos;requestStatusAndPosition&apos;</source>
        <translation type="unfinished">Fehler beim Aufruf von  &apos;requestStatusAndPosition&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="363"/>
        <source>Warning while calling &apos;requestStatusAndPosition&apos;</source>
        <translation type="unfinished">Warnung beim Aufruf von  &apos;requestStatusAndPosition&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="384"/>
        <source>setActuatorInterrupt can only be called for actuator plugins</source>
        <translation type="unfinished">Der Parameter &apos;setActuatorInterrupt&apos; kann nur für Motor-Plugins aufgerufen werden</translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1520"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für Pflichtparameter!</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1524"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für optionale Parameter!</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1528"/>
        <source>uninitialized vector for output parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für Ausgabeparameter!</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1451"/>
        <source>Constructor must be overwritten</source>
        <translation type="unfinished">Der Konstruktor muss überschrieben sein</translation>
    </message>
</context>
<context>
    <name>ito::AddInBase</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="511"/>
        <source>function execution unused in this plugin</source>
        <translation type="unfinished">Die Funktion &apos;execution&apos; wird in diesem Plugin nicht benutzt</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="563"/>
        <source>Toolbox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="722"/>
        <source>function with this name is already registered.</source>
        <translation type="unfinished">Es existiert bereits eine Funktion mit diesem Namen.</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="781"/>
        <source>Your plugin is supposed to have a configuration dialog, but you did not implement the showConfDialog-method</source>
        <translation type="unfinished">Das Plugin hat vermutlich einen Konfigurationsdialog, aber die Methode &apos;showConfDialog&apos; wurde nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInDataIO</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="854"/>
        <source>listener does not have a slot </source>
        <translation type="unfinished">&apos;Listener&apos; hat keinen Slot </translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="858"/>
        <source>this object already has been registered as listener</source>
        <translation type="unfinished">Diese Objekt wurde bereits beim &apos;Listener&apos; registriert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="872"/>
        <source>timer could not be set</source>
        <translation type="unfinished">Timer kann nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="903"/>
        <source>the object could not been removed from the listener list</source>
        <translation type="unfinished">Das Objekt kann nicht aus der &apos;Listener&apos;-Liste entfernt werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1001"/>
        <source>empty interval buffer has been given</source>
        <translation type="unfinished">Es wurde ein leerer Bereichspuffer übergeben</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1039"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1059"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1077"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1095"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1113"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1131"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1149"/>
        <location filename="../../common/sources/addInInterface.cpp" line="1167"/>
        <source>not implemented</source>
        <translation type="unfinished">Nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1045"/>
        <source>method startDevice() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;startDevice&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1063"/>
        <source>method stopDevice() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;stopDevice&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1081"/>
        <source>method acquire() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;acquire&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1099"/>
        <source>method stop() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;stop&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1117"/>
        <source>method getVal(void*, ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;getVal(void*, ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1135"/>
        <source>method getVal(QSharedPointer&lt;char&gt;, QSharedPointer&lt;int&gt;, ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;getVal(QSharedPointer&lt;char&gt;, QSharedPointer&lt;int&gt;, ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1153"/>
        <source>method copyVal(void*,ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;copyVal(void*,ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1171"/>
        <source>method setVal(const char*, const int, ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;setVal(const char*, const int, ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInGrabber</name>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="89"/>
        <location filename="../../common/sources/addInGrabber.cpp" line="104"/>
        <source>slot &apos;setSource&apos; of live source node could not be invoked</source>
        <translation type="unfinished">Der Slot &apos;setSource&apos; kann nicht aufgerufen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="236"/>
        <source>Error during check data, external dataObject invalid. Object has more or less than 1 plane. It must be of right size and type or an uninitilized image.</source>
        <translation type="unfinished">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt hat mehr oder weniger als eine Ebene. Es muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="240"/>
        <location filename="../../common/sources/addInGrabber.cpp" line="263"/>
        <source>Error during check data, external dataObject invalid. Object must be of right size and type or an uninitilized image.</source>
        <translation type="unfinished">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
</context>
</TS>
