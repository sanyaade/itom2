"""Prints the string "Hello World" and exits."""

myText = "Hello World"


def myFunc():
    """Prints the text"""
    print(myText)


if __name__ == "__main__":
    myFunc()
